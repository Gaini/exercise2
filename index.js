const EventEmitter = require('events');
const message = require('./message');
const finish = require('./finish');
const finishvk = require('./finishvk');

class ChatApp extends EventEmitter {
    /**
     * @param {String} title
     */
    constructor(title) {
        super();

        this.title = title;

        // Посылать каждую секунду сообщение
        setInterval(() => {
            this.emit('message', `${this.title}: ping-pong`);
        }, 1000);
    }

    close() {
        this.emit('close');
    }

}
let webinarChat = new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat = new ChatApp('---------vk');

vkChat.setMaxListeners(2);

let PrepareOnMessage = () => {
    console.log('Готовлюсь к ответу');
};

webinarChat.on('message', PrepareOnMessage);
vkChat.on('message', PrepareOnMessage);

webinarChat.on('message', message);
facebookChat.on('message', message);
vkChat.on('message', message);


finishvk(vkChat, 'Закрываю вконтакте...', 10000)
    .then(files => vkChat.close());
finish(facebookChat, 'Закрываю фейсбук, все внимание — вебинару!', 15000);
finish(webinarChat, 'Закрываю вебинар...', 30000);

let close = () => {
    console.log('Чат вконтакте закрылся :(');
};

vkChat.on('close', close);