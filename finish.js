function finish(subject, text, time) {

    setTimeout(() => {
        console.log(text);
        subject.removeAllListeners();
    }, time);

}

module.exports = finish;
