function finish(subject, text, time) {

    return new Promise(function (resolve, reject) {
        setTimeout(() => {
            console.log(text);
            subject.removeAllListeners();
            resolve(true);
        }, time);
    });
}

module.exports = finish;
